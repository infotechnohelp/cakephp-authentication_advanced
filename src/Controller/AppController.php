<?php

declare(strict_types=1);

namespace Infotechnohelp\Authentication\Controller;

use App\Controller\AppController as BaseController;

/**
 * Class AppController
 * @package Infotechnohelp\Authentication\Controller
 */
class AppController extends BaseController
{
    /**
     * Initialize
     */
    public function initialize()
    {
        parent::initialize();
    }
}
