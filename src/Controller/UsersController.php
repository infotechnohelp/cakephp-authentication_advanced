<?php

declare(strict_types=1);

namespace Infotechnohelp\Authentication\Controller;

use Infotechnohelp\Authentication\Controller\Traits\IdentifyUser;
use Cake\Core\Configure;
use Cake\Routing\Router;

class UsersController extends AppController
{
    use IdentifyUser;

    /**
     * Initialize
     */
    public function initialize()
    {
        parent::initialize();
        $this->Auth->allow();
    }

    public function login()
    {
        $user = $this->identifyUser();

        $redirectQueryParameter = $this->getRequest()->getQuery('redirect');

        $redirectUrl = (empty($redirectQueryParameter)) ? '/' : Router::url($redirectQueryParameter, true);

        $AuthenticationConfig = Configure::read('Infotechnohelp.Authentication') ?? [];

        if (array_key_exists('loggedInAction', $AuthenticationConfig) && $redirectUrl === '/') {
            $redirectUrl = $AuthenticationConfig['loggedInAction'];
        }

        if ($user) {
            $this->Auth->setUser($user);

            $redirectQueryParameter  =$this->getRequest()->getQuery('redirect');

            if (!empty($redirectQueryParameter)) {
                $this->redirect($redirectQueryParameter);
            }

            $this->redirect($redirectUrl);
        }

        $query = $this->getRequest()->getQuery();

        if (!array_key_exists('authenticationFailed', $query)) {
            $query['authenticationFailed'] = '';
        }

        $this->redirect(explode('?', $this->getRequest()->referer())[0] . '?' . http_build_query($query));
    }

    public function logout()
    {
        $this->Auth->logout();

        $redirectQueryParameter = $this->getRequest()->getQuery('redirect');

        $config = Configure::read('Infotechnohelp.Authentication');

        if (array_key_exists('loggedOutAction', $config) && empty($redirectQueryParameter)) {
            $this->redirect($config['loggedOutAction']);
        }

        $this->redirect('/' . $redirectQueryParameter);
    }
}
