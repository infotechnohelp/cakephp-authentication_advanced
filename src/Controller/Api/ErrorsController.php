<?php

declare(strict_types=1);

namespace Infotechnohelp\Authentication\Controller\Api;

use Cake\Http\Exception\ForbiddenException;

class ErrorsController extends AppController
{
    public function initialize()
    {
        parent::initialize();
        $this->Auth->allow();
    }

    public function notAllowed()
    {
        throw new ForbiddenException('You have no permissions to visit a requested location:' .
            $this->getRequest()->getQuery('redirect'));
    }
}
