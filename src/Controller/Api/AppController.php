<?php

declare(strict_types=1);

namespace Infotechnohelp\Authentication\Controller\Api;

use App\Controller\Api\AppController as BaseController;

/**
 * Class AppController
 * @package Infotechnohelp\Authentication\Controller\Api
 */
class AppController extends BaseController
{
    /**
     * Initialize
     */
    public function initialize()
    {
        parent::initialize();
    }
}
