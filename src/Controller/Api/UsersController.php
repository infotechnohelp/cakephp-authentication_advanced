<?php

declare(strict_types=1);

namespace Infotechnohelp\Authentication\Controller\Api;

use App\ModelManager\UserEmailConfirmations;
use Infotechnohelp\Authentication\Model\Entity\User;
use Infotechnohelp\Authentication\Model\Entity\UserRole;
use Infotechnohelp\Authentication\Controller\Traits\IdentifyUser;
use Cake\Event\Event;
use Cake\Http\Exception\BadRequestException;
use Cake\ORM\TableRegistry;

class UsersController extends \App\Controller\Api\AppController
{
    use IdentifyUser;

    /**
     * Initialize
     */
    public function initialize()
    {
        parent::initialize();
        $this->Auth->allow();
    }

    public function login()
    {
        $user = $this->identifyUser();

        if ($user) {

            // @todo TMP solution, move inside plugin later
            $confirmedEntity = (new UserEmailConfirmations())->first([], 'UserId', $user['id']);

            if(empty($confirmedEntity)){
                throw new BadRequestException('Email is not confirmed');
            }

            $confirmed = $confirmedEntity->get('confirmed');

            if(!$confirmed){
                throw new BadRequestException('Email is not confirmed');
            }

            $this->Auth->setUser($user);

            $this->_setResponse($user);

            return;
        }

        throw new BadRequestException('User cannot be identified');
    }

    public function logout()
    {
        $this->Auth->logout();

        $this->_setResponse([true]);
    }

    public function register()
    {
        $UsersTable = TableRegistry::getTableLocator()->get('Infotechnohelp/Authentication.Users');
        $requestData = $this->_getRequestData();

        if (!isset($requestData['password']) || empty($requestData['password'])) {
            throw new BadRequestException('Please provide password');
        }

        if (!isset($requestData['repeat-password']) || empty($requestData['repeat-password'])) {
            throw new BadRequestException('Please repeat password');
        }

        if ($requestData['password'] !== $requestData['repeat-password']) {
            throw new BadRequestException('Passwords do not match');
        }

        $userRoleId = (empty($requestData['user_role_id'])) ? null : (int)$requestData['user_role_id'];

        if ($userRoleId !== UserRole::USER && $userRoleId !== null && !$this->Auth->user()) {
            throw new BadRequestException('You are not allowed to register a new user with this user role');
        }

        // @todo TMP solution
        if(empty($requestData['username'])){
            unset($requestData['username']);
        }

        /** @var  User $User */
        $User = $UsersTable->newEntity(array_merge($requestData, [
            'user_role_id' => $userRoleId,
        ]));

        $result = $UsersTable->saveOrFail($User);

        $result->unsetProperty('repeat-password');

        $this->_setResponse($result);
    }

    /**
     * @param \Cake\Event\Event $event
     *
     * @return void
     */
    public function beforeFilter(Event $event)
    {
        $action = $event->getSubject()->getRequest()->getParam('action');

        if ($action === 'logout') {
            $this->request->allowMethod('get');
        } else {
            $this->request->allowMethod('post');
        }
    }
}
