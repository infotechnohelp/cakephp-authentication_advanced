# cakephp-authentication_<basic/advanced>

## Implementation

### `composer require infotechnohelp/cakephp-authentication_advanced`

### 1. Add plugin

##### src/Application.php → bootstrap()

`$this->addPlugin('Infotechnohelp/Authentication', ['routes' => true]);`

or use terminal

`bin/cake plugin load Infotechnohelp/Authentication -r`

### 2. Add component

##### src/Controller/AppController.php → initialize()

`$this->loadComponent('Infotechnohelp/Authentication.Authentication');`


### 3. Set authentication config

`config/bootstrap.php`

Configure::write('Infotechnohelp.Authentication', [
    'loginAction' => '/',
    'loggedInAction' => '/demo-controller/authenticated-action',
    'loggedOutAction' => '/?loggedOut',
]);


### 4. Migrate migrations

`bin/cake migrations migrate --plugin Infotechnohelp/Authentication`

Tables: Users, UserRoles

### 5. Add user roles

### 6. Set fixtures for testing purposes

`composer.json`

```
	...,
    "autoload-dev": {
        "psr-4": {
            "Infotechnohelp\\Authentication\\Test\\": "vendor/infotechnohelp/cakephp-authentication_advanced/tests"
        }
    },
	...
```

Do not forget to refresh autoload data

`composer dump-autoload`

Add fixtures into a TestCase

`IntegrationTestCase`

```
    public $fixtures = [
        'plugin.Infotechnohelp/Authentication.Users',
        'plugin.Infotechnohelp/Authentication.UserRoles',
    ];
```

Login as a user

`$this->session(['Auth' => ['User' => ['id' => 1]]]);`


## Usage

### Terminal

#### Register a new user

`bin/cake Infotechnohelp/Authentication.users register <username> <password> <user_role_id>`

### Usage in controllers

By default, all controllers will have a forbidden access. 

In order to allow a controller's method:

```
    public function initialize()
    {
        parent::initialize();
        $this->Auth->allow('allowedMethod');
    }
```

In order to allow all methods:

`$this->Auth->allow();`

### API

* APP/authentication/api/register → username, password, repeat-password (email, user_role_id)

User may provide either a username or an email to log in, authentication layer will automatically detect it.

`{"data": User entity, "message": null}`

`{"data":null, "message": message text}`

* APP/authentication/api/login → username or email (automatic detection), password

`{"data":User entity, "message": null}`

`{"data":null, "message": Error message text}`

* APP/authentication/api/logout

`{"data":true, "message": null}`

### Authentication tunnels

* APP/authentication/login?redirect=<redirection url> → username or email (automatic detection), password

```html
        <form action="[app root]/authentication/login?redirect=[redirect url]" method="post">
            Username: <input name="username" type="text">
            Password: <input name="password" type="password">
            <button type="submit">Log in</button>
        </form>
```

In case redirection url is not specified, a default value will be used. Configure::read('Infotechnohelp.Authentication')['loggedInAction'].

User may provide either a username or an email to log in, authentication layer will automatically detect it.

