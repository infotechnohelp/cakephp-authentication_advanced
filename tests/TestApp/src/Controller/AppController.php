<?php

namespace App\Controller;

use Cake\Controller\Controller;

class AppController extends Controller
{
    /**
     * Initialize
     */
    public function initialize()
    {
        parent::initialize();

        $this->loadComponent('Infotechnohelp/Authentication.Authentication');
    }
}