<form method="POST" action="<?= \Cake\Routing\Router::url('/') ?>auth-api">
    <fieldset>
        <label for="username"><?= __('Username') ?></label><input type="text" name="username"/>
        <label for="password"><?= __('Password') ?></label><input type="password" name="password"/>
    </fieldset>
    <button type="submit">Log in</button>
</form>

<form method="POST" action="<?= \Cake\Routing\Router::url('/') ?>auth-api/logout">
    <button type="submit">Log out</button>
</form>



<form method="POST" action="<?= \Cake\Routing\Router::url('/') ?>auth-api/register">
    <fieldset>
        <label for="username"><?= __('Username') ?></label><input type="text" name="username"/>
        <label for="password"><?= __('Password') ?></label><input type="password" name="password"/>
        <label for="repeat-password"><?= __('Repeat password') ?></label>
        <input type="password" name="repeat-password"/>
    </fieldset>
    <button type="submit">Register</button>
</form>