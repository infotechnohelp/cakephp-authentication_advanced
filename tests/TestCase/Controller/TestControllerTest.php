<?php

namespace Infotechnohelp\Authentication\Test\TestCase\Controller;

use Cake\Core\Configure;
use Cake\TestSuite\IntegrationTestCase;

class TestControllerTest extends IntegrationTestCase
{
    public $fixtures = [
        'plugin.Infotechnohelp/Authentication.Users',
        'plugin.Infotechnohelp/Authentication.UserRoles',
    ];

    public function testAllowedAction()
    {
        $this->get('tests/allowed-action');
        $this->assertResponseOk();
    }

    public function testForbiddenAction()
    {
        Configure::write('Infotechnohelp.Authentication', [
            'loginAction' => '/log-in',
        ]);

        $this->get('tests/forbidden-action');
        $this->assertRedirect('/log-in?redirect=%2Ftests%2Fforbidden-action');
        //$this->_response->getHeaderLine('Location');
    }

    public function testForbiddenActionLoggedIn()
    {
        $this->session(['Auth' => ['User' => ['id' => 1]]]);
        $this->get('tests/forbidden-action');
        $this->assertResponseOk();
    }

    public function testTunnelWithRedirectParam()
    {
        $this->post('authentication/login?redirect=tests/forbidden-action', [
            'username' => 'Username',
            'password' => '1234',
        ]);

        $this->assertRedirect('tests/forbidden-action');
    }

    public function testTunnelWrongPassword()
    {
        $this->post('authentication/login?redirect=tests/forbidden-action', [
            'username' => 'Username',
            'password' => 'wrongPassword',
        ]);

        $this->assertRedirect('/?redirect=tests%2Fforbidden-action&authenticationFailed=');
    }

    public function testTunnelWrongPasswordReferrer()
    {
        // Mock referrer
        $_SERVER['HTTP_REFERER'] = 'referrer';

        $this->post('authentication/login?redirect=tests/forbidden-action', [
            'username' => 'Username',
            'password' => 'wrongPassword',
        ]);

        $this->assertRedirect('/referrer?redirect=tests%2Fforbidden-action&authenticationFailed=');
    }

    public function testTunnelWithConfig()
    {
        Configure::write('Infotechnohelp.Authentication', [
            'loggedInAction' => '/logged-in',
        ]);

        $this->post('authentication/login', [
            'username' => 'Username',
            'password' => '1234',
        ]);

        $this->assertRedirect('logged-in');
    }

    public function testTunnelWithEmailAsUsername()
    {
        $this->post('authentication/login?redirect=tests/forbidden-action', [
            'username' => 'email@email.com',
            'password' => '1234',
        ]);

        $this->assertRedirect('tests/forbidden-action');

        $this->assertEquals('Username', $this->_requestSession->read('Auth.User')['username']);
    }
}
